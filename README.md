# Util

[![CI Status](http://img.shields.io/travis/Junaid Akram/Util.svg?style=flat)](https://travis-ci.org/Junaid Akram/Util)
[![Version](https://img.shields.io/cocoapods/v/Util.svg?style=flat)](http://cocoapods.org/pods/Util)
[![License](https://img.shields.io/cocoapods/l/Util.svg?style=flat)](http://cocoapods.org/pods/Util)
[![Platform](https://img.shields.io/cocoapods/p/Util.svg?style=flat)](http://cocoapods.org/pods/Util)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

Util is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "Util"
```

## Author

Junaid Akram, junaidakr@exceedgulf.com

## License

Util is available under the MIT license. See the LICENSE file for more info.
