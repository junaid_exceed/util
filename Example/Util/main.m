//
//  main.m
//  Util
//
//  Created by Junaid Akram on 01/01/2017.
//  Copyright (c) 2017 Junaid Akram. All rights reserved.
//

@import UIKit;
#import "JAAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([JAAppDelegate class]));
    }
}
