//
//  JAAppDelegate.h
//  Util
//
//  Created by Junaid Akram on 01/01/2017.
//  Copyright (c) 2017 Junaid Akram. All rights reserved.
//

@import UIKit;

@interface JAAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
