//
//  UtilBaseEntity.h
//  Pods
//
//  Created by Junaid Akram on 26/01/2017.
//
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface UtilBaseEntity : NSObject
{
    @private
    BOOL _isInserted;
    BOOL _isUpdated;
    BOOL _isDeleted;
}

@property (nonatomic, strong) NSMutableDictionary *changedKeys;

-(NSMutableDictionary*)recordAsDictionary;

+(NSDictionary*)properties;
+(NSDictionary*)propertiesInLowerCaseWithPropertieDictionary:(NSDictionary*)properties;

+(NSString*)tableName;

@property (nonatomic, strong) NSNumber *objectID;

+(id)objectIDColumnName;

-(void)willChangeValueForKey:(NSString*)key;
-(void)didChangeValueForKey:(NSString*)key;

-(id)valueForKey:(NSString*)key;
-(void)setValue:(id)value forKey:(NSString*)key;

-(NSDictionary*)changedValues;
-(BOOL)hasChanges;

+(id)objectFormDictionary:(NSDictionary*)dictionary withKey:(NSString*)key;
+(NSString*)replaceNilString:(NSString*)string;
+(NSDate*)dateFromString:(NSString*)dateString;

@end
