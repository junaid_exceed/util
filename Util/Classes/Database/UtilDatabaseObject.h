//
//  UtilDatabaseObject.h
//  Pods
//
//  Created by Junaid Akram on 26/01/2017.
//
//

#import <Foundation/Foundation.h>
#import <FMDB/FMDB.h>

@interface UtilDatabaseObject : NSObject

+(instancetype)sharedDatabaseObject;
+(void)deleteDatabase;
-(FMDatabase*)database;
+(NSString*)fileName;


@end
