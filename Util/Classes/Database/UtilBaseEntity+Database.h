//
//  UtilBaseEntity+Database.h
//  Pods
//
//  Created by Junaid Akram on 26/01/2017.
//
//

#import "UtilBaseEntity.h"
#import "UtilDatabaseObject.h"

@interface UtilBaseEntity (Database)

+(UtilDatabaseObject*)database;

+ (instancetype)objectByResultSet:(FMResultSet*)results;

#pragma mark - Status

- (BOOL)isInserted;
- (BOOL)isUpdated;
- (BOOL)isDeleted;

#pragma mark - CRUD

- (BOOL)updateObject;
- (BOOL)insertObject;
- (BOOL)saveObject;
- (BOOL)deleteObject;
+ (BOOL)deleteAll;
+ (BOOL)deleteObjectWithWhereString:(id)whereString andParameterDictionary:(NSDictionary *)arguments;

+ (instancetype)selectObject:(id)objectID;
//http://www.cocoawithlove.com/2008/03/core-data-one-line-fetch.html
+ (NSMutableArray *)selectObjectsWithWhereString:(id)whereString andParameterDictionary:(NSDictionary *)arguments;
+ (NSMutableArray *)selectAll;
+ (NSMutableArray *)selectAllVisible;

+ (NSInteger )countObjects;
+ (NSInteger )countObjectsWithWhereString:(id)whereString;


#pragma mark - Validation
//  validation is a critical piece of functionality and the following methods are likely the most commonly overridden methods in custom subclasses
- (BOOL)validateForDelete:(NSError **)error;
- (BOOL)validateForInsert:(NSError **)error;
- (BOOL)validateForUpdate:(NSError **)error;

#pragma mark - FMDB Transactions.
typedef void (^fmdb_transactions_block_t)(void);
void fmdb_transactions(fmdb_transactions_block_t block ,UtilDatabaseObject* database);

@end
