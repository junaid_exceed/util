//
//  UtilBaseEntity+Database.m
//  Pods
//
//  Created by Junaid Akram on 26/01/2017.
//
//

#import "UtilBaseEntity+Database.h"
#import "UtilConstants.h"

static NSString *databaseObjectLoc = @"databaseObjectLoc" ;

@implementation UtilBaseEntity (Database)

+(UtilDatabaseObject*)database{
    return [UtilDatabaseObject sharedDatabaseObject];
}

#pragma mark - Status
- (BOOL)isInserted{
    return _isInserted;
}

- (BOOL)isUpdated{
    return _isUpdated;
}

- (BOOL)isDeleted{
    return _isDeleted;
}

-(void)setIsSelected{
    _isInserted = YES;
    _isUpdated = YES;
}


#pragma mark - CRUD

- (BOOL)updateObject{
    @synchronized(databaseObjectLoc) {
        
        NSError *error = nil;
        if (![self validateForUpdate:&error]) {
            UtilLogError(@"not validate For Insert Update: %@",error);
        }
        
        NSLog(@"BaseEntity updateObject");
        FMDatabase *db  = [[[self class]database] database];
        NSMutableString * updateString = [NSMutableString string];
        [updateString appendFormat:@"update %@ set ",[[self class] tableName]];
        
        NSArray * allProperties = [self.changedValues allKeys];
        NSMutableDictionary *dictionaryArgs = [NSMutableDictionary dictionary];
        
        for (NSString* properte in allProperties) {
            NSObject* val = [self valueForKey:properte];
            if (val != nil) {
                NSString *colName = [[[self class] properties] objectForKey:properte];
                if (colName !=nil) {
                    [updateString appendFormat:@" %@ = :%@ ,",colName,colName];
                    [dictionaryArgs setObject:val forKey:colName];
                }
            }
        }
        
        if ([dictionaryArgs count] > 0) {
            [updateString deleteCharactersInRange:NSMakeRange([updateString length]-1, 1)];
        }
        
        //where
        [updateString appendFormat:@"where %@ = :%@ ",[[self class] objectIDColumnName],[[self class] objectIDColumnName]];
        [dictionaryArgs setObject:[self objectID] forKey:[[self class] objectIDColumnName]];
        
        BOOL result =  [db executeUpdate:updateString withParameterDictionary:dictionaryArgs];
        if (!result) {
            UtilLogError(@"error %@",[db lastError]);
        }else{
            _isUpdated = YES;
        }
        return result;
    }
}
- (BOOL)insertObject{
    @synchronized(databaseObjectLoc) {
        
        NSError *error = nil;
        if (![self validateForInsert:&error]) {
            UtilLogError(@"not validate For Insert Error: %@",error);
        }
        
        NSLog(@"BaseEntity insertObject");
        FMDatabase *db  = [[[self class]database] database];
        NSMutableString * insertString = [NSMutableString string];
        [insertString appendFormat:@"INSERT INTO %@ ( ",[[self class] tableName]];
        
        NSArray * allProperties = [[[self class] properties] allKeys];
        NSMutableArray *arrayArgs = [NSMutableArray array];
        
        for (NSString* properte in allProperties) {
            NSObject* val = [self valueForKey:properte];
            
            if (val != nil) {
                [insertString appendFormat:@" %@ ,",[[[self class] properties] objectForKey:properte]];
                [arrayArgs addObject:val];
            }
        }
        
        if ([arrayArgs count] > 0) {
            [insertString deleteCharactersInRange:NSMakeRange([insertString length]-1, 1)];
        }
        
        [insertString appendString:@") values ("];
        for (int i = 0; i < [arrayArgs count] ; i++) {
            [insertString appendString:@"?,"];
        }
        if ([arrayArgs count] > 0) {
            [insertString deleteCharactersInRange:NSMakeRange([insertString length]-1, 1)];
        }
        [insertString appendString:@")"];
        
        BOOL result =  [db executeUpdate:insertString withArgumentsInArray:arrayArgs];
        if (!result) {
            UtilLogError(@"error %@",[db lastError]);
        }else{
            //lastInsertRowId
            long long int lastId = [db lastInsertRowId];
            [self setObjectID:[NSNumber numberWithLongLong:lastId]];
            
            _isInserted = YES;
            _isUpdated = YES;
        }
        return result;
    }
}
- (BOOL)saveObject{
    BOOL result =  NO;
    if (_isInserted) {
        if (!_isUpdated) {
            result = [self updateObject];
        }else {
            result = YES;
        }
    }else{
        result = [self insertObject];
    }
    return result;
}

+ (BOOL)deleteAll
{
    //    DELETE * FROM table_name;
    
    @synchronized(databaseObjectLoc) {
        UtilLog(@"BaseEntity deleteObject : %@",arguments);
        FMDatabase *db  = [[[self class]database] database];
        NSMutableString * deleteString = [NSMutableString string];
        [deleteString appendFormat:@"delete from %@ ",[[self class] tableName]];
        
        BOOL result =  [db executeUpdate:deleteString withParameterDictionary:nil];
        if (!result) {
            UtilLogError(@"error %@",[db lastError]);
        }else{
            //_isDeleted = YES;
        }
        return result;
    }
}

- (BOOL)deleteObject{
    //where
    NSMutableString *deleteString = [NSMutableString string];
    [deleteString appendFormat:@"where %@ = :%@ ",[[self class] objectIDColumnName],[[self class] objectIDColumnName]];
    if ([self objectID]) {
        BOOL result = [[self class] deleteObjectWithWhereString:deleteString andParameterDictionary:@{[[self class] objectIDColumnName]:[self objectID]}];
        if (result) {
            _isDeleted = YES;
        }
        return result;
    }else
    {
        return NO;
    }
    
}

+ (BOOL)deleteObjectWithWhereString:(id)whereString andParameterDictionary:(NSDictionary *)arguments{
    @synchronized(databaseObjectLoc) {
        UtilLog(@"BaseEntity deleteObject : %@",arguments);
        FMDatabase *db  = [[[self class]database] database];
        NSMutableString * deleteString = [NSMutableString string];
        [deleteString appendFormat:@"delete from %@ ",[[self class] tableName]];
        
        if (whereString) {
            [deleteString appendString:whereString];
        }
        
        BOOL result =  [db executeUpdate:deleteString withParameterDictionary:arguments];
        if (!result) {
            UtilLogError(@"error %@",[db lastError]);
        }else{
            //_isDeleted = YES;
        }
        return result;
    }
    
}

+ (instancetype)objectByResultSet:(FMResultSet*)results{
    UtilBaseEntity* obj = nil;
    Class cls = [self class];
    obj = [[cls alloc] init];
    NSArray * allProperties = [[[self class] properties] allKeys];
    for (NSString* properte in allProperties) {
        NSString *colName = [[[self class] properties] objectForKey:properte];
        id  val = [results objectForColumnName:colName];
        if (val !=  [NSNull null]) {
            [obj setValue:val forKey:properte];
        }
    }
    //object ID
    id  val = [results objectForColumnName:[[self class] objectIDColumnName]];
    if (val !=  [NSNull null]) {
        [obj setObjectID:val];
    }
    return obj;
}

+ (instancetype)selectObject:(id)objectID{
    @synchronized(databaseObjectLoc) {
        UtilLog(@"BaseEntity selectObject");
        FMDatabase *db  = [[[self class]database] database];
        NSMutableString * selectString = [NSMutableString string];
        [selectString appendFormat:@"select %@.* from %@ ",[[self class] tableName],[[self class] tableName]];
        NSMutableDictionary *dictionaryArgs = [NSMutableDictionary dictionary];
        
        //where
        [selectString appendFormat:@"where %@ = :%@ ",[[self class] objectIDColumnName],[[self class] objectIDColumnName]];
        [dictionaryArgs setObject:objectID forKey:[[self class] objectIDColumnName]];
        
        FMResultSet* results = [db executeQuery:selectString withParameterDictionary:dictionaryArgs];
        id obj = nil;
        if([results next]) {
            obj = [self objectByResultSet:results];
            [obj setIsSelected];
        }
        return obj;
    }
}
+ (NSInteger )countObjectsWithWhereString:(id)whereString {
    
    @synchronized(databaseObjectLoc) {
        FMDatabase *db  = [[[self class]database] database];
        NSMutableString * selectString = [NSMutableString string];
        [selectString appendFormat:@"SELECT COUNT(%@) from %@ ",[[self class] objectIDColumnName],[[self class] tableName]];
        if (whereString) {
            [selectString appendString:whereString];
        }
        
        NSUInteger count  = [db intForQuery:selectString];
        return count;
    }
}

+ (NSInteger )countObjects{
    
    @synchronized(databaseObjectLoc) {
        FMDatabase *db  = [[[self class]database] database];
        NSMutableString * selectString = [NSMutableString string];
        [selectString appendFormat:@"SELECT COUNT(%@) from %@ ",[[self class] objectIDColumnName],[[self class] tableName]];
        
        
        NSUInteger count  = [db intForQuery:selectString];
        return count;
    }
}

+ (NSMutableArray *)selectAll{
    return [self selectObjectsWithWhereString:nil andParameterDictionary:nil];
}

+ (NSMutableArray *)selectAllVisible{
    return [self selectObjectsWithWhereString:@"Where is_member_hidden = 0 " andParameterDictionary:nil];
}


+ (NSMutableArray *)selectObjectsWithWhereString:(id)whereString andParameterDictionary:(NSDictionary *)arguments{
    @synchronized(databaseObjectLoc) {
        UtilLog(@"BaseEntity selectObjectsWithWhereString : %@",whereString);
        FMDatabase *db  = [[[self class]database] database];
        NSMutableString * selectString = [NSMutableString string];
        [selectString appendFormat:@"select %@.* from %@ ",[[self class] tableName],[[self class] tableName]];
        
        if (whereString) {
            [selectString appendString:whereString];
        }
        
        FMResultSet* results = [db executeQuery:selectString withParameterDictionary:arguments];
        NSMutableArray  *objects = [NSMutableArray array];
        while([results next]) {
            id obj = nil;
            obj = [self objectByResultSet:results];
            [obj setIsSelected];
            [objects addObject:obj];
        }
        return objects;
    }
}

#pragma mark - Validation
//  validation is a critical piece of functionality and the following methods are likely the most commonly overridden methods in custom subclasses
- (BOOL)validateForDelete:(NSError **)error{
    return YES;
}
- (BOOL)validateForInsert:(NSError **)error{
    return YES;
}
- (BOOL)validateForUpdate:(NSError **)error{
    return YES;
}



#pragma mark - FMDB Transactions.
void fmdb_transactions(fmdb_transactions_block_t block ,UtilDatabaseObject* database){
    FMDatabase *fmdb = [database database];
    
    [fmdb beginTransaction];
    @try
    {
        block();
        [fmdb commit];
    }
    @catch(NSException* e)
    {
        [fmdb rollback];
        UtilLogError(@"fmdb transactions error: %@",e);
    }
}

@end
