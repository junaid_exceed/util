//
//  UtilBaseEntity.m
//  Pods
//
//  Created by Junaid Akram on 26/01/2017.
//
//

#import <objc/runtime.h>

#import "UtilBaseEntity.h"
static NSString *objectIDProperty = @"objectID";
static NSString *objectIDColumn = @"object_id";

@implementation UtilBaseEntity
{
    NSMutableDictionary *data;
}

-(instancetype)init
{
    self = [super init];
    if (self) {
        data = [NSMutableDictionary new];
        self.changedKeys = [NSMutableDictionary dictionary];
        
        _isDeleted = FALSE;
        _isInserted = FALSE;
        _isUpdated = FALSE;
    }
    return self;
}

-(NSString*)description
{
    return [data description];
}

-(NSMutableDictionary*)recordAsDictionary
{
    NSMutableDictionary *dictionary = [NSMutableDictionary new];
    @autoreleasepool {
        unsigned int numberOfProperties = 0;
        objc_property_t *propertyArray = class_copyPropertyList([self class], &numberOfProperties);
        
        for (NSUInteger i = 0; i < numberOfProperties; i++)
        {
            objc_property_t property = propertyArray[i];
            NSString *key = [NSString stringWithUTF8String:property_getName(property)];
            dictionary[key] = data[[key lowercaseString]];
        }
        free(propertyArray);
    }
    
    return dictionary;
}

+(NSDictionary*)properties
{
    NSLog(@"Must override entity Properties");
    return nil;
}

+(NSDictionary*)propertiesInLowerCaseWithPropertieDictionary:(NSDictionary*)properties
{
    NSDictionary *sharedPropertiesInLowerCase;
    NSDictionary *dict = properties;
    if ([dict count] > 0) {
        NSArray *keys = [dict allKeys];
        NSMutableDictionary *newDic = [NSMutableDictionary new];
        for (NSString *key in keys) {
            newDic[[key lowercaseString]] = [dict[key] lowercaseString];
        }
        sharedPropertiesInLowerCase = newDic;
    }
    return sharedPropertiesInLowerCase;
}

+(NSString*)tableName
{
    NSLog(@"Must override entity Properties");
    return nil;
}

-(NSMethodSignature*)methodSignatureForSelector:(SEL)aSelector
{
    NSString *sel = NSStringFromSelector(aSelector);
    if ([sel rangeOfString:@"set"].location == 0) {
        return [NSMethodSignature signatureWithObjCTypes:"v@:@"];
    }
    else {
        return [NSMethodSignature signatureWithObjCTypes:"@@:"];
    }
}

-(void)forwardInvocation:(NSInvocation *)anInvocation
{
    NSString *key = NSStringFromSelector([anInvocation selector]);
    if ([key rangeOfString:@"set"].location == 0) {
        key = [key substringWithRange:NSMakeRange(3, [key length]-4)] ;
        //http://stackoverflow.com/questions/16044817/strange-zombie-in-forwardinvocation-getargumentatindex-methods
        void *temp;
        [anInvocation getArgument:&temp atIndex:2];
        NSObject *options = (__bridge NSObject *)temp;
        [self setValue:options forKey:key];
    }
    else {
        NSObject *obj = [self valueForKey:key];
        [anInvocation setReturnValue:&obj];
    }
}

-(void)setObjectID:(NSNumber *)objectID
{
    [self setValue:objectID forKey:objectIDProperty];
}

-(id)objectID
{
    if ([[self class] objectIDColumnName]) {
        return [self valueForKey:objectIDProperty];
    }
    return nil;
}

+(id)objectIDColumnName
{
    return objectIDColumn;
}

-(void)willChangeValueForKey:(NSString *)key
{
    
}

-(void)didChangeValueForKey:(NSString *)key
{
    id value = [self valueForKey:key];
    if (value) {
        self.changedKeys[key] = value;
    }
    _isUpdated = NO;
}

-(id)valueForKey:(NSString *)key
{
    if (key != objectIDProperty) {
        if ([self respondsToSelector:NSSelectorFromString(key)]) {
            SEL selector = NSSelectorFromString(key);
            if (selector) {
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
                return [self performSelector:selector];
            }
        }
    }
    
    key = [key lowercaseString];
    NSString *obj = data[key];
    return obj;
}

-(void)setValue:(id)value forKey:(NSString *)key
{
    key = [key lowercaseString];
    [self willChangeValueForKey:key];
    
    if (!value) {
        [data removeObjectForKey:key];
    }
    else{
        data[key] = value;
    }
    
    [self didChangeValueForKey:key];
}

-(NSDictionary*)changedValues
{
    return self.changedKeys;
}

-(BOOL)hasChanges
{
    return [self.changedKeys count] > 0;
}

+(id)objectFormDictionary:(NSDictionary *)dictionary withKey:(NSString *)key
{
    if (dictionary && ![dictionary isEqual:[NSNull null]] && [dictionary isKindOfClass:[NSDictionary class]]) {
        id obj = dictionary[key];
//        NSLog(@"%@", [obj class]);
        if ([obj isKindOfClass:[NSString class]]) {
            if ([obj isEqualToString:@"<null>"]) {
                obj  = @"";
            }
        }
        return (obj == nil || [obj isEqual:[NSNull null]]) ? @"" : obj ;
    }
    else{
        NSLog(@"Bug was here");
    }
    return @"";
}

+(NSString*)replaceNilString:(NSString*)string
{
    return (string == nil || [string isEqual:[NSNull null]]) ? @"" : string ;
}

+(NSDate*)dateFromString:(NSString*)dateString
{
    if (dateString.length == 0) {
        return nil;
    }
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd\'T\'HH:mm:ssZ"];
    
    NSRange range = [dateString rangeOfString:@"."];
    if (range.location != NSNotFound) {
        dateString = [dateString
                      stringByReplacingCharactersInRange:NSMakeRange(range.location, dateString.length-range.location)
                      withString:@"Z"];
    }
    
    NSDate* date = [dateFormatter dateFromString:dateString];
    return date;
}

@end
