//
//  UtilDatabaseObject.m
//  Pods
//
//  Created by Junaid Akram on 26/01/2017.
//
//

#import "UtilDatabaseObject.h"
#import "Util.h"

@implementation UtilDatabaseObject

static id sharedDatabaseObject = nil;

+(instancetype)sharedDatabaseObject
{
    @synchronized(self) {
        if (sharedDatabaseObject == nil){
            Class databaseObjectClass = NSClassFromString(@"DatabaseObject");
            sharedDatabaseObject = [[databaseObjectClass alloc]init];
        }
    }
    return sharedDatabaseObject;
}

+(void)deleteDatabase
{
    @synchronized (self) {
        NSString *documentDirectory = [Util applicationDocumentsDirectory];
        NSString *filePath = [documentDirectory stringByAppendingPathComponent:[self fileName]];
        
        if ([[NSFileManager defaultManager] fileExistsAtPath:filePath]) {
            [[NSFileManager defaultManager] removeItemAtPath:filePath error:nil];
        }
        
        sharedDatabaseObject = nil;
    }
}

+ (NSString*)fileName
{
    NSLog(@"must overrid database in IDNDatabaseObject");
    return nil;
}

- (FMDatabase*)database{
    NSLog(@"must overrid database in IDNDatabaseObject");
    return nil;
}

@end
