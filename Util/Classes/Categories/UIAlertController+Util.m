//
//  UIAlertController+Util.m
//  Pods
//
//  Created by Junaid Akram on 26/01/2017.
//
//

#import "UIAlertController+Util.h"

@implementation UIAlertController (Util)

+(void)showAlertWithMessage:(NSString*)message inViewController:(UIViewController*)viewController
{
    [self showAlertWithTitle:NULL message:message firstActionTitle:@"Ok" firstActionStyle:UIAlertActionStyleDestructive firstActionhandler:nil inViewController:viewController];
}

+(void)showAlertWithTitle:(NSString*)title message:(NSString*)message firstActionTitle:(NSString*)firstActionTitle firstActionStyle:(UIAlertActionStyle)firstActionStyle  firstActionhandler:(void(^)(UIAlertAction *action))firstActionhandler inViewController:(UIViewController*)viewController
{
    
    [self showAlertForTwoActionsWithTitle:title message:message firstActionTitle:firstActionTitle firstActionStyle:firstActionStyle firstActionhandler:firstActionhandler secondActionTitle:nil secondActionStyle:0 secondActionhandler:nil inViewController:viewController];
}

+(void)showAlertForTwoActionsWithTitle:(NSString*)title message:(NSString*)message firstActionTitle:(NSString*)firstActionTitle firstActionStyle:(UIAlertActionStyle)firstActionStyle  firstActionhandler:(void(^)(UIAlertAction *action))firstActionhandler secondActionTitle:(NSString*)secondActionTitle secondActionStyle:(UIAlertActionStyle)secondActionStyle  secondActionhandler:(void(^)(UIAlertAction *action))secondActionhandler inViewController:(UIViewController*)viewController
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    if (firstActionTitle) {
        UIAlertAction *action = [UIAlertAction actionWithTitle:firstActionTitle style:firstActionStyle handler:^(UIAlertAction * _Nonnull action) {
            if (firstActionhandler) {
                firstActionhandler(action);
            }
        }];
        
        [alert addAction:action];
    }
    
    if (secondActionTitle) {
        UIAlertAction *action = [UIAlertAction actionWithTitle:secondActionTitle style:secondActionStyle handler:^(UIAlertAction * _Nonnull action) {
            if (secondActionhandler) {
                secondActionhandler(action);
            }
        }];
        
        [alert addAction:action];
    }
    
    [viewController presentViewController:alert animated:YES completion:nil];
}

#pragma mark - Action Sheet

+(void)showActionSheetForTwoActionsAndCancelWithTitle:(NSString*)title message:(NSString*)message firstActionTitle:(NSString*)firstActionTitle firstActionStyle:(UIAlertActionStyle)firstActionStyle  firstActionhandler:(void(^)(UIAlertAction *action))firstActionhandler secondActionTitle:(NSString*)secondActionTitle secondActionStyle:(UIAlertActionStyle)secondActionStyle  secondActionhandler:(void(^)(UIAlertAction *action))secondActionhandler inViewController:(UIViewController*)viewController
{
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleActionSheet];
    if (firstActionTitle) {
        UIAlertAction *action = [UIAlertAction actionWithTitle:firstActionTitle style:firstActionStyle handler:^(UIAlertAction * _Nonnull action) {
            if (firstActionhandler) {
                firstActionhandler(action);
            }
        }];
        
        [actionSheet addAction:action];
    }
    
    if (secondActionTitle) {
        UIAlertAction *action = [UIAlertAction actionWithTitle:secondActionTitle style:secondActionStyle handler:^(UIAlertAction * _Nonnull action) {
            if (secondActionhandler) {
                secondActionhandler(action);
            }
        }];
        
        [actionSheet addAction:action];
    }
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
    
    [actionSheet addAction:cancelAction];
    
    [viewController presentViewController:actionSheet animated:YES completion:nil];
}

@end
