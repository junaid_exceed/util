//
//  UIViewController+Keyboard.m
//  Pods
//
//  Created by Junaid Akram on 22/02/2017.
//
//

#import "UIViewController+Keyboard.h"
#import <objc/runtime.h>

@implementation UIViewController (Keyboard)

#pragma mark - properties

NSString const *topConstraintKey = @"topConstraint.key";

-(void)setTopConstraint:(NSLayoutConstraint *)topConstraint
{
    objc_setAssociatedObject(self, &topConstraintKey, topConstraint, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

-(NSLayoutConstraint*)topConstraint
{
    return objc_getAssociatedObject(self, &topConstraintKey);
}

////////

NSString const *Keyboard_OffsetKey = @"Keyboard_Offset.key";

-(void)setKeyboard_Offset:(NSNumber*)Keyboard_Offset
{
    objc_setAssociatedObject(self, &Keyboard_OffsetKey, Keyboard_Offset, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    if ([Keyboard_Offset doubleValue] == 0) {
        [self setViewMovedUp:NO withAnimationDuration:0.5];
    }
    else{
        [self setViewMovedUp:YES withAnimationDuration:0.5];
    }
}

-(NSNumber*)Keyboard_Offset
{
    return objc_getAssociatedObject(self, &Keyboard_OffsetKey);
}

////////

NSString const *topConstraintDefaultConstantValueKey = @"topConstraintDefaultConstantValue.key";

-(void)setTopConstraintDefaultConstantValue:(NSNumber *)topConstraintDefaultConstantValue
{
    objc_setAssociatedObject(self, &topConstraintDefaultConstantValueKey, topConstraintDefaultConstantValue, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

-(NSNumber*)topConstraintDefaultConstantValue
{
    return objc_getAssociatedObject(self, &topConstraintDefaultConstantValueKey);
}


#pragma mark - View Life Cyle

-(void)addKeyboardObservation
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillBeHidden:) name:UIKeyboardWillHideNotification object:nil];
}

-(void)removeKeyboardObservation
{
    @try {
        [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardDidShowNotification object:nil];
    } @catch (NSException *exception) {
        
    }
    
    @try {
        [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
    } @catch (NSException *exception) {
        
    }
    
    @try {
        [[NSNotificationCenter defaultCenter] removeObserver:self];
    } @catch (NSException *exception) {
        
    }
}

- (void)keyboardWillShow:(NSNotification*)aNotification
{
    [self setViewMovedUp:YES withAnimationDuration:0.5];
}

- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    [self setViewMovedUp:NO withAnimationDuration:0.5];
}

-(void)setViewMovedUp:(BOOL)movedUp withAnimationDuration:(CGFloat)animationDuration
{
    __weak typeof(self) weakSelf = self;
    
    BOOL shouldAnimate = FALSE;
    
    if (movedUp && self.topConstraint.constant != -[self.Keyboard_Offset doubleValue]) {
        shouldAnimate = TRUE;
        if ([self.Keyboard_Offset doubleValue] == 0) {
            self.topConstraint.constant = [self.topConstraintDefaultConstantValue doubleValue];
        }
        else{
            self.topConstraint.constant = -[self.Keyboard_Offset doubleValue];
        }
        
    }
    else if(!movedUp){
        shouldAnimate = TRUE;
        self.topConstraint.constant = [self.topConstraintDefaultConstantValue doubleValue];
    }
    
    if (shouldAnimate) {
        [UIView animateWithDuration:animationDuration animations:^{
            
            [weakSelf.view layoutIfNeeded];
        } completion:nil];
    }
}

@end
