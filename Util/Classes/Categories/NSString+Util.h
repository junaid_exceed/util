//
//  NSString+Util.h
//  Pods
//
//  Created by Junaid Akram on 01/01/2017.
//
//

#import <Foundation/Foundation.h>

@interface NSString (Util)

- (BOOL)hasPrefix:(NSString *)prefix caseInsensitive:(BOOL)caseInsensitive;

- (BOOL)isJustWhiteSpace;

#pragma mark -- URLEncoding

-(NSString *)urlEncodeForChractersSting:(NSString*)chracterString UsingEncoding:(NSStringEncoding)encoding;

-(BOOL) isValidEmail;

-(BOOL) isValidWebURL;

-(BOOL) isValidSocialURL;

-(BOOL)isValidPhone;

@end
