//
//  NSObject+Util.m
//  Pods
//
//  Created by Junaid Akram on 06/01/2017.
//
//

#import "NSObject+Util.h"
#import "BlocksKit.h"
#import <objc/runtime.h>
#ifndef __IPHONE_4_0
#import <dispatch/dispatch.h>
#endif


@implementation NSObject (Blocks)


-(void)ng_runBlock:(void (^)(void))block
{
    block();
}

-(void)performBlockInBackground:(void (^)(void))block onComplete:(void (^)(void))completeBlock {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT,0), ^{
        if (block) {
            block();
        }
        if (completeBlock) {
            [self performSelectorOnMainThread:@selector(ng_runBlock:) withObject:[completeBlock copy] waitUntilDone:NO];
        }
    });
}


-(void)performBlockOnMainThread:(void (^)(void))block waitUntilDone:(BOOL)complete {
    [self performSelectorOnMainThread:@selector(ng_runBlock:) withObject:block waitUntilDone:complete];
}

@end

@interface NSObject ()

-(NSMutableDictionary *)bk_observerBlocks;

@end

@implementation NSObject (Util)


- (void)util_addObserverForKeyPath:(NSString*)keyPath identifier:(NSString*)identifier task:(void(^)(id obj, NSDictionary *change))task
{
    [self bk_addObserverForKeyPath:keyPath identifier:[NSString stringWithFormat:@"%@_%@", identifier, keyPath] options:NSKeyValueObservingOptionNew task:task];
}

- (void)util_removeObserverWithIdentifier:(NSString*)tokenString
{
    NSParameterAssert(tokenString);
    
    NSMutableDictionary *dict;
    
    @synchronized (self) {
        id selfID = self;
        dict = [selfID bk_observerBlocks];
        if (!dict) {
            return;
        }
    }
    
    NSMutableArray *theKeys = [NSMutableArray new];
    for (NSString *key in dict.allKeys) {
        if ([key hasPrefix:[NSString stringWithFormat:@"%@_", tokenString]]) {
            [theKeys addObject:key];
        }
    }
    
    for (NSString *key in theKeys) {
        [self bk_removeObserversWithIdentifier:key];
    }
}

@end

