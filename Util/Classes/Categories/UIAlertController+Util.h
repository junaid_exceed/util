//
//  UIAlertController+Util.h
//  Pods
//
//  Created by Junaid Akram on 26/01/2017.
//
//

#import <UIKit/UIKit.h>

@interface UIAlertController (Util)

+(void)showAlertWithMessage:(NSString*)message inViewController:(UIViewController*)viewController;

+(void)showAlertWithTitle:(NSString*)title message:(NSString*)message firstActionTitle:(NSString*)firstActionTitle firstActionStyle:(UIAlertActionStyle)firstActionStyle  firstActionhandler:(void(^)(UIAlertAction *action))firstActionhandler inViewController:(UIViewController*)viewController;

+(void)showAlertForTwoActionsWithTitle:(NSString*)title message:(NSString*)message firstActionTitle:(NSString*)firstActionTitle firstActionStyle:(UIAlertActionStyle)firstActionStyle  firstActionhandler:(void(^)(UIAlertAction *action))firstActionhandler secondActionTitle:(NSString*)secondActionTitle secondActionStyle:(UIAlertActionStyle)secondActionStyle  secondActionhandler:(void(^)(UIAlertAction *action))secondActionhandler inViewController:(UIViewController*)viewController;

#pragma mark - Action Sheet

+(void)showActionSheetForTwoActionsAndCancelWithTitle:(NSString*)title message:(NSString*)message firstActionTitle:(NSString*)firstActionTitle firstActionStyle:(UIAlertActionStyle)firstActionStyle  firstActionhandler:(void(^)(UIAlertAction *action))firstActionhandler secondActionTitle:(NSString*)secondActionTitle secondActionStyle:(UIAlertActionStyle)secondActionStyle  secondActionhandler:(void(^)(UIAlertAction *action))secondActionhandler inViewController:(UIViewController*)viewController;

@end
