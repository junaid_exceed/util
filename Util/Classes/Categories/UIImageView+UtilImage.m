//
//  UIImageView+UtilImage.m
//  Pods
//
//  Created by Junaid Akram on 27/08/2017.
//
//

#import "UIImageView+UtilImage.h"
#import <objc/runtime.h>
#import "AFNetworking.h"
#import "AFImageDownloader.h"
#import "NSThread+Util.h"
#import "UtilImageLocalCache.h"

#pragma mark -

@interface UIImageView (_UtilImage)

@property (readwrite, nonatomic, strong, setter = af_setActiveImageDownloadReceipt:) AFImageDownloadReceipt *af_activeImageDownloadReceipt;

@end

@implementation UIImageView (_UtilImage)

- (AFImageDownloadReceipt *)af_activeImageDownloadReceipt {
    return (AFImageDownloadReceipt *)objc_getAssociatedObject(self, @selector(af_activeImageDownloadReceipt));
}

- (void)af_setActiveImageDownloadReceipt:(AFImageDownloadReceipt *)imageDownloadReceipt {
    objc_setAssociatedObject(self, @selector(af_activeImageDownloadReceipt), imageDownloadReceipt, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

@end

@implementation UIImageView (UtilImage)

+ (AFImageDownloader *)sharedImageDownloader {
    
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wgnu"
    return objc_getAssociatedObject(self, @selector(sharedImageDownloader)) ?: [AFImageDownloader defaultInstance];
#pragma clang diagnostic pop
}

+ (void)setSharedImageDownloader:(AFImageDownloader *)imageDownloader {
    objc_setAssociatedObject(self, @selector(sharedImageDownloader), imageDownloader, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}



#pragma mark -

- (void)setimageWithURL:(NSURL *)url {
    [self setimageWithURL:url placeholderImage:nil];
}

- (void)setimageWithURL:(NSURL *)url
       placeholderImage:(UIImage *)placeholderImage
{
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPShouldHandleCookies:NO];
    [request addValue:@"image/*" forHTTPHeaderField:@"Accept"];
    
    [self setimageWithURLRequest:request placeholderImage:placeholderImage success:nil failure:nil];
}

- (void)setimageWithURLRequest:(NSURLRequest *)urlRequest
              placeholderImage:(UIImage *)placeholderImage
                       success:(void (^)(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image))success
                       failure:(void (^)(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error))failure
{
    if ([urlRequest URL] == nil) {
        [self cancelImageDownloadTask];
        self.image = placeholderImage;
        return;
    }
    
    if ([self isActiveTaskURLEqualToURLRequest:urlRequest]){
        return;
    }
    
    [self cancelImageDownloadTask];
    
    AFImageDownloader *downloader = [[self class] sharedImageDownloader];
    
    UIImage *cachedImage = [[UtilImageLocalCache sharedImageLocalCache] imageFromKey:urlRequest.URL.absoluteString];
    if (cachedImage) {
        if (success) {
            success(nil, nil, cachedImage);
        }
        
        __weak __typeof(self)weakSelf = self;
        [NSThread performBlockOnMainThread:^{
            weakSelf.image = cachedImage;
        }];
        
        [self clearActiveDownloadInformation];
    } else {
        self.image = placeholderImage;
        
        AFImageResponseSerializer* serializer = (AFImageResponseSerializer*)downloader.sessionManager.responseSerializer;
        serializer.acceptableContentTypes = [serializer.acceptableContentTypes setByAddingObject:@"image/svg+xml"];
        serializer.acceptableContentTypes = [serializer.acceptableContentTypes setByAddingObject:@"image/jpg"];

        
        __weak __typeof(self)weakSelf = self;
        NSUUID *downloadID = [NSUUID UUID];
        AFImageDownloadReceipt *receipt;
        receipt = [downloader
                   downloadImageForURLRequest:urlRequest
                   withReceiptID:downloadID
                   success:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, UIImage * _Nonnull responseObject) {
                       __strong __typeof(weakSelf)strongSelf = weakSelf;
                       if ([strongSelf.af_activeImageDownloadReceipt.receiptID isEqual:downloadID]) {
                           
                           if (responseObject) {
                               CGFloat height = strongSelf.frame.size.height * 3;
                               responseObject = [self resizeImage:responseObject MaxWidthHeight:height];
                           }

                           
                           if (success) {
                               success(request, response, responseObject);
                           } else if(responseObject) {
                               strongSelf.image = responseObject;
                           }
                           
                           [NSThread performBlockOnMainThread:^{
                               weakSelf.image = responseObject;
                           }];
                           
                           [[UtilImageLocalCache sharedImageLocalCache] storeImage:responseObject forKey:urlRequest.URL.absoluteString toDisk:YES];
                           
                           [strongSelf clearActiveDownloadInformation];
                       }
                       
                   }
                   failure:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, NSError * _Nonnull error) {
                       __strong __typeof(weakSelf)strongSelf = weakSelf;
                       if ([strongSelf.af_activeImageDownloadReceipt.receiptID isEqual:downloadID]) {
                           NSString *url = request.URL.absoluteString;
                           NSLog(@"%@", url);
                           if (failure) {
                               failure(request, response, error);
                           }
                           [strongSelf clearActiveDownloadInformation];
                       }
                   }];
        
        self.af_activeImageDownloadReceipt = receipt;
    }
}

- (void)cancelImageDownloadTask {
    if (self.af_activeImageDownloadReceipt != nil) {
        [[self.class sharedImageDownloader] cancelTaskForImageDownloadReceipt:self.af_activeImageDownloadReceipt];
        [self clearActiveDownloadInformation];
    }
}

- (void)clearActiveDownloadInformation {
    self.af_activeImageDownloadReceipt = nil;
}

- (BOOL)isActiveTaskURLEqualToURLRequest:(NSURLRequest *)urlRequest {
    return [self.af_activeImageDownloadReceipt.task.originalRequest.URL.absoluteString isEqualToString:urlRequest.URL.absoluteString];
}

- (UIImage*)resizeImage:(UIImage*)image MaxWidthHeight:(CGFloat)maxWidthHeight
{
    UIImage *resizeImage;
    
    CGSize resizeSize = CGSizeZero;
    CGSize imageSize = image.size;
    if (imageSize.width == imageSize.height && imageSize.width > maxWidthHeight) {
        resizeSize = CGSizeMake(maxWidthHeight, maxWidthHeight);
    }
    else if (imageSize.width > imageSize.height && imageSize.width > maxWidthHeight){
        CGFloat oldWidth = imageSize.width;
        CGFloat scaleFactor = maxWidthHeight / oldWidth;
        
        CGFloat newHeight = imageSize.height * scaleFactor;
        CGFloat newWidth = oldWidth * scaleFactor;
        
        resizeSize = CGSizeMake(newWidth, newHeight );
    }
    else if (imageSize.height > imageSize.width && imageSize.height > maxWidthHeight){
        
        CGFloat oldHeight = imageSize.height;
        CGFloat scaleFactor = maxWidthHeight / oldHeight;
        CGFloat newWidth = imageSize.width * scaleFactor;
        CGFloat newHeight = oldHeight * scaleFactor;
        
        resizeSize = CGSizeMake(newWidth, newHeight );
    }
    
    if (resizeSize.width == 0 && resizeSize.height == 0) {
        resizeImage = image;
    }
    else{
        resizeImage = [self image:image byScalingAndCroppingForSize:resizeSize];
        //        resizeImage = [image resizedImage:resizeSize interpolationQuality:kCGInterpolationMedium];
    }
    
    return resizeImage;
}


- (UIImage*)image:(UIImage*)image byScalingAndCroppingForSize:(CGSize)targetSize
{
    UIImage *sourceImage = image;
    UIImage *newImage = nil;
    CGSize imageSize = sourceImage.size;
    CGFloat width = imageSize.width;
    CGFloat height = imageSize.height;
    CGFloat targetWidth = targetSize.width;
    CGFloat targetHeight = targetSize.height;
    CGFloat scaleFactor = 0.0;
    CGFloat scaledWidth = targetWidth;
    CGFloat scaledHeight = targetHeight;
    CGPoint thumbnailPoint = CGPointMake(0.0,0.0);
    
    if (CGSizeEqualToSize(imageSize, targetSize) == NO)
    {
        CGFloat widthFactor = targetWidth / width;
        CGFloat heightFactor = targetHeight / height;
        
        if (widthFactor > heightFactor)
        {
            scaleFactor = widthFactor; // scale to fit height
        }
        else
        {
            scaleFactor = heightFactor; // scale to fit width
        }
        
        scaledWidth  = width * scaleFactor;
        scaledHeight = height * scaleFactor;
        
        // center the image
        if (widthFactor > heightFactor)
        {
            thumbnailPoint.y = (targetHeight - scaledHeight) * 0.5;
        }
        else
        {
            if (widthFactor < heightFactor)
            {
                thumbnailPoint.x = (targetWidth - scaledWidth) * 0.5;
            }
        }
    }
    
    //UIGraphicsBeginImageContext(targetSize); // this will crop
    UIGraphicsBeginImageContextWithOptions(targetSize, NO, 0.0);
    CGRect thumbnailRect = CGRectZero;
    thumbnailRect.origin = thumbnailPoint;
    thumbnailRect.size.width  = scaledWidth;
    thumbnailRect.size.height = scaledHeight;
    
    [sourceImage drawInRect:thumbnailRect];
    
    newImage = UIGraphicsGetImageFromCurrentImageContext();
    
    if(newImage == nil)
    {
        //        IDNLog(@"could not scale image");
        NSLog(@"could not scale image");
    }
    
    //pop the context to get back to the default
    UIGraphicsEndImageContext();
    
    return newImage;
}


@end
