//
//  NSObject+Util.h
//  Pods
//
//  Created by Junaid Akram on 06/01/2017.
//
//

#import <Foundation/Foundation.h>

@interface NSObject (Blocks)
-(void)performBlockInBackground:(void (^)(void))block onComplete:(void (^)(void))completeBlock;
-(void)performBlockOnMainThread:(void (^)(void))block waitUntilDone:(BOOL)complete;
@end

@interface NSObject (Util)

- (void)util_addObserverForKeyPath:(NSString*)keyPath identifier:(NSString*)identifier task:(void(^)(id obj, NSDictionary *change))task;

- (void)util_removeObserverWithIdentifier:(NSString*)tokenString;

@end

