//
//  NSString+Util.m
//  Pods
//
//  Created by Junaid Akram on 01/01/2017.
//
//

#import "NSString+Util.h"

@implementation NSString (Util)

- (BOOL)hasPrefix:(NSString *)prefix caseInsensitive:(BOOL)caseInsensitive
{
    if (!caseInsensitive)
    return [self hasPrefix:prefix];
    
    const NSStringCompareOptions options = NSAnchoredSearch|NSCaseInsensitiveSearch;
    NSRange prefixRange = [self rangeOfString:prefix options:options];
    return prefixRange.location == 0 && prefixRange.length > 0;
}

-(BOOL)isJustWhiteSpace
{
    BOOL isJustWhiteSpace = FALSE;
    NSCharacterSet *set = [NSCharacterSet whitespaceCharacterSet];
    if ([[self stringByTrimmingCharactersInSet: set] length] == 0)
    {
        isJustWhiteSpace = TRUE;
    }
    
    return isJustWhiteSpace;
}


#pragma mark -- URLEncoding

-(NSString *)urlEncodeForChractersSting:(NSString*)chracterString UsingEncoding:(NSStringEncoding)encoding
{
    NSString *chractersToEscape = chracterString;
    
    if (!chractersToEscape) {
        chractersToEscape = @"!*'\"();:@&=+$,/?%#[]% ";
    }
    
    NSCharacterSet *characterSet = [NSCharacterSet characterSetWithCharactersInString:chractersToEscape];
    NSString *url = [self stringByAddingPercentEncodingWithAllowedCharacters:characterSet];
    return url;
}

-(BOOL) isValidEmail
{
    BOOL stricterFilter = NO; // Discussion http://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:self];
}

-(BOOL) isValidWebURL
{
    NSString *urlRegEx = @"((www\\.|(http|https)+\\:\\/\\/)[&#95;a-z0-9-]+\\.[a-z0-9\\/&#95;_:@=.+?,##%&~-]*[^.|\\'|\\# |!|\(|?|,| |>|<|;|\\)])";
    NSPredicate *urlTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", urlRegEx];
    return [urlTest evaluateWithObject:self];
}

-(BOOL) isValidSocialURL
{
    NSString *urlRegEx = @"((www\\.|(http|https)+\\:\\/\\/)?[&#95;a-z0-9-]+\\.[a-z0-9\\/&#95;_:@=.+?,##%&~-]*[^.|\'|\\# |!|\(|?|,| |>|<|;|\\)])";
    NSPredicate *urlTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", urlRegEx];
    return [urlTest evaluateWithObject:self];
}

-(BOOL)isValidPhone
{
    NSString *phoneRegex = @"[\\+]?[0-9.-]+";
    NSPredicate *phoneTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", phoneRegex];
    return [phoneTest evaluateWithObject:self];
}

@end
