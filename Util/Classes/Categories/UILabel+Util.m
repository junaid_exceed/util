//
//  UILabel+Util.m
//  Pods
//
//  Created by Junaid Akram on 01/01/2017.
//
//

#import "UILabel+Util.h"
#import "NSString+Util.h"

@implementation UILabel (Util)

- (void)colorPrefix:(NSString*)prefix withColor:(UIColor*)color
{
    NSArray *labelTextComponents = [self.text componentsSeparatedByString:@" "];
    
    NSArray *prefixComponents = [prefix componentsSeparatedByString:@" "];
    
    for (int i = 0; i < labelTextComponents.count; i++) {
        NSString *labelComponent = labelTextComponents[i];
        for (NSString *prefixComponent in prefixComponents) {
            if ([labelComponent hasPrefix:prefixComponent caseInsensitive:YES]) {
                NSRange rangeOfPrefix = [labelComponent rangeOfString:prefixComponent options:NSCaseInsensitiveSearch];
                if (i > 0) {
                    NSString *combineStringTillLastComponent = [self combineStringFromArray:labelTextComponents tillIndex:(i-1)];
                    rangeOfPrefix = NSMakeRange(rangeOfPrefix.location + combineStringTillLastComponent.length, rangeOfPrefix.length);
                }
                [self coloredRange:rangeOfPrefix withColor:color];
            }
        }
    }
}

#pragma mark - Internal Methods

-(NSString*)combineStringFromArray:(NSArray*)componentsArray tillIndex:(int)index
{
    NSMutableString *mutableString = [NSMutableString new];
    
    [mutableString appendString:componentsArray[0]];
    [mutableString appendString:@" "];
    
    int i = 1;
    while (i <= index) {
        [mutableString appendString:componentsArray[i]];
        [mutableString appendString:@" "];
        i++;
    }
    return mutableString;
}

- (void) coloredRange: (NSRange) range withColor:(UIColor*)color
{
    if (![self respondsToSelector:@selector(setAttributedText:)]) {
        return;
    }
    NSMutableAttributedString *attributedText = [[NSMutableAttributedString alloc] initWithString:self.text];
    [attributedText addAttributes:@{NSForegroundColorAttributeName:color,
                                    NSFontAttributeName:[UIFont boldSystemFontOfSize:self.font.pointSize]}
                            range:range];
    
    self.attributedText = attributedText;
}

@end
