//
//  UILabel+Util.h
//  Pods
//
//  Created by Junaid Akram on 01/01/2017.
//
//

#import <UIKit/UIKit.h>

@interface UILabel (Util)

- (void)colorPrefix:(NSString*)prefix withColor:(UIColor*)color;

@end
