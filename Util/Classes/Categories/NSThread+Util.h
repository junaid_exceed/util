//
//  NSThread+Util.h
//  Pods
//
//  Created by Junaid Akram on 26/08/2017.
//
//

#import <Foundation/Foundation.h>

@interface NSThread (Util)

+(void)detachNewThreadSelector:(SEL)selector toTarget:(id)target withObjects:(id)firstObject,... NS_REQUIRES_NIL_TERMINATION;

-(void)performBlock:(void (^)(void))block;
-(void)performBlock:(void (^)(void))block waitUntilDone:(BOOL)wait;

+(void)performBlockInBackground:(void (^)(void))block;
+(void)performBlockInBackground:(void (^)(void))block afterDelay:(NSTimeInterval)delay;
+(void)performBlockOnMainThread:(void (^)(void))block;
+(void)performBlockOnMainThread:(void (^)(void))block afterDelay:(NSTimeInterval)delay;

@end
