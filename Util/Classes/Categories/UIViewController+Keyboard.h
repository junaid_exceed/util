//
//  UIViewController+Keyboard.h
//  Pods
//
//  Created by Junaid Akram on 22/02/2017.
//
//

#import <UIKit/UIKit.h>

@interface UIViewController (Keyboard)

@property (nonatomic, strong) NSLayoutConstraint *topConstraint;
@property (nonatomic, strong) NSNumber *Keyboard_Offset;
@property (nonatomic, strong) NSNumber *topConstraintDefaultConstantValue;

-(void)addKeyboardObservation;
-(void)removeKeyboardObservation;

@end
