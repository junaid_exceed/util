//
//  UIColor+Util.h
//  Pods
//
//  Created by Junaid Akram on 26/01/2017.
//
//

#import <UIKit/UIKit.h>

@interface UIColor (Util)

+(UIColor*)util_colorWithRed:(int)red green:(int)green blue:(int)blue;
+(UIColor*)util_colorWithRed:(int)red green:(int)green blue:(int)blue alpha:(float)alpha;

+ (UIColor *)colorWithDictionary:(NSDictionary *)dictionary;
+ (UIColor *)colorWithRGBString:(NSString *)rgb;
+ (UIColor *)colorWithHSBString:(NSString *)hsb;
+ (UIColor *)colorWithHexString:(NSString *)hex;
+ (UIColor *)colorWithName:(NSString *)name;

@end
