//
//  NSThread+Util.m
//  Pods
//
//  Created by Junaid Akram on 26/08/2017.
//
//

#import "NSThread+Util.h"

@implementation NSThread (Util)

+(void)detachNewThreadSelector:(SEL)selector toTarget:(id)target withObjects:(id)firstObject,... {
    NSMutableArray * arguments = [NSMutableArray array];
    // Add the selector
    [arguments addObject:NSStringFromSelector(selector)];
    // Add the target
    [arguments addObject:target];
    // Add the arguments
    if (firstObject)
    {
        [arguments addObject:firstObject];
        
        id argument;
        va_list objectList;
        va_start(objectList, firstObject);
        
        while (argument = va_arg(objectList, id))
        {
            [arguments addObject:argument];
        }
        
        va_end(objectList);
    }
    
    NSThread* thread = [[NSThread alloc] initWithTarget:[NSThread class] selector:@selector(runThreadWithArgArray:) object:arguments];
    [thread start];
}


+(void)runThreadWithArgArray:(NSArray*)args {
    SEL selector = NSSelectorFromString([args objectAtIndex:0]);
    id target = [args objectAtIndex:1];
    NSMethodSignature * signature = [target methodSignatureForSelector:selector];
    NSInvocation* invocation = [NSInvocation invocationWithMethodSignature:signature];
    [invocation setTarget:target];
    [invocation setSelector:selector];
    NSInteger i = 2; // self, _cmd, ...
    if ([args count] > 2) {
        NSInteger index = 2;
        for (index = 2; index < [args count]; index++) {
            id argument = [args objectAtIndex:index];
            [invocation setArgument:&argument atIndex:i++];
        }
    }
    [invocation retainArguments];
    @autoreleasepool {
        [invocation invoke];
    }
}


- (void)performBlock:(void (^)())block
{
    if ([[NSThread currentThread] isEqual:self])
        block();
    else
        [self performBlock:block waitUntilDone:NO];
}
- (void)performBlock:(void (^)())block waitUntilDone:(BOOL)wait
{
    [NSThread performSelector:@selector(ng_runBlock:)
                     onThread:self
                   withObject:[block copy]
                waitUntilDone:wait];
}
+ (void)ng_runBlock:(void (^)())block
{
    block();
}

+(void)performBlockInBackground:(void (^)())block{
#ifdef __IPHONE_4_0
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT,0), ^{
        block();
    });
#else
    [NSThread performSelectorInBackground:@selector(ng_runBlock:)
                               withObject:[[block copy] autorelease]];
#endif
}


+(void)performBlockInBackground:(void (^)())block afterDelay:(NSTimeInterval)delay {
    double delayInSeconds = delay;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT,0), ^(void){
        block();
    });
}


+ (void)performBlockOnMainThread:(void (^)())block{
#ifdef __IPHONE_4_0
    dispatch_async(dispatch_get_main_queue(), ^{
        block();
    });
#else
    [NSThread performSelectorOnMainThread:@selector(ng_runBlock:)
                               withObject:[[block copy] autorelease]];
#endif
}

+(void)performBlockOnMainThread:(void (^)())block afterDelay:(NSTimeInterval)delay {
    double delayInSeconds = delay;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        block();
    });
}

@end
