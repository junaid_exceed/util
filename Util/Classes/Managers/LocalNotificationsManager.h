//
//  LocalNotificationsManager.h
//  Pods
//
//  Created by Junaid Akram on 28/04/2017.
//
//

#import <Foundation/Foundation.h>

@interface LocalNotificationsManager : NSObject

+(instancetype)shared;

-(void)registerLocalNotifications;

-(void)postNotification:(NSString*)title withAction:(NSString*)actionTitle onDate:(NSDate*)fireDate uniqueKey:(NSString*)key;
-(void)postNotification:(NSString*)title withAction:(NSString*)actionTitle onDate:(NSDate*)fireDate uniqueKey:(NSString*)key withCustomParameters:(NSDictionary*)customParameter;
-(void)postNotification:(NSString*)title withAction:(NSString*)actionTitle onDate:(NSDate*)fireDate uniqueKey:(NSString*)key withCustomParameters:(NSDictionary*)customParameter andBadgeNumber:(NSInteger)badgeNumber;

-(void)postNotification:(NSString*)title withAction:(NSString*)actionTitle onDate:(NSDate*)fireDate uniqueKey:(NSString*)key withCustomParameters:(NSDictionary*)customParameter andBadgeNumber:(NSInteger)badgeNumber repeat:(BOOL)repeat repeatUnit:(NSCalendarUnit)repeatUnit;

-(void)cancelNotificationWithKey:(NSString*)key;
-(void)cancelAllLocalNotifications;

-(void)handleNotificationWithKey:(NSString*)key onComplete:(void(^)(NSDictionary* params))onComplete;

-(void)localNotificationReceived:(NSDictionary*)notificationDict;

@end
