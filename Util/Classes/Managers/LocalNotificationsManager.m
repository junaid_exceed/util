//
//  LocalNotificationsManager.m
//  Pods
//
//  Created by Junaid Akram on 28/04/2017.
//
//

#import "LocalNotificationsManager.h"
#import "UtilMacros.h"
#import <UserNotifications/UserNotifications.h>


@interface LocalNotificationsManager() <UNUserNotificationCenterDelegate>

@property(strong, nonatomic) NSMutableDictionary* handlers;
@property(strong, nonatomic) NSMutableArray* notificationsReceived;

@end

@implementation LocalNotificationsManager

SYNTHESIZE_ARC_SINGLETON_FOR_CLASS(LocalNotificationsManager);

-(void)registerLocalNotifications
{
    if (([[[UIDevice currentDevice] systemVersion] floatValue] >= 10.0)) {
        UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
        center.delegate = self;
        [center requestAuthorizationWithOptions:(UNAuthorizationOptionBadge | UNAuthorizationOptionSound | UNAuthorizationOptionAlert) completionHandler:^(BOOL granted, NSError * _Nullable error) {
            if (!error) {
                [[UIApplication sharedApplication] registerForRemoteNotifications];
            }
        }];
    }
    else{
        UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:
                                                (UIUserNotificationTypeBadge
                                                 |UIUserNotificationTypeSound
                                                 |UIUserNotificationTypeAlert) categories:nil];
        [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
        [[UIApplication sharedApplication]  registerForRemoteNotifications];
    }
}

- (NSMutableDictionary *) handlers {
    if(_handlers == nil) {
        _handlers = [NSMutableDictionary dictionary];
    }
    return _handlers;
}

- (NSMutableArray *) notificationsReceived {
    if(_notificationsReceived == nil) {
        _notificationsReceived = [NSMutableArray array];
    }
    return _notificationsReceived;
}

-(instancetype)init
{
    self = [super init];
    if (self) {
        
    }
    
    return self;
}

-(void)localNotificationReceived:(NSDictionary*)notificationDict;
{
    [self.notificationsReceived addObject:notificationDict];
    
    [self dispatchNotificationHandlers:notificationDict];
}

-(void)dispatchNotificationHandlers:(NSDictionary*)notificationDict
{
    NSString *key = notificationDict[@"key"];
    for (NSDictionary *userInfo in self.notificationsReceived) {
        if ([key isEqualToString:userInfo[@"key"]]) {
            NSMutableDictionary* handlerDict = self.handlers[userInfo[@"key"]];
            if (handlerDict && ![handlerDict[@"handled"] boolValue]) {
                void(^handler)(NSDictionary* params) = handlerDict[@"handler"];
                if (handler) {
                    handler(userInfo);
                }
                handlerDict[@"handled"] = @"1";
            }
        }
    }
}

//-(void)dispatchNotificationHandlers
//{
//    for (UILocalNotification * notif in self.notificationsReceived) {
//        NSMutableDictionary* handlerDict = self.handlers[notif.userInfo[@"key"]];
//        if (handlerDict && ![handlerDict[@"handled"] boolValue]) {
//            //            void(^handler)(NSDictionary* params) = (void(^handler)(NSDictionary* params)) handlerDict[@"handler"];
//            void(^handler)(NSDictionary* params) = handlerDict[@"handler"];
//            if (handler) {
//                handler(notif.userInfo);
//            }
//            handlerDict[@"handled"] = @"1";
//        }
//    }
//}

-(void)handleNotificationWithKey:(NSString*)key onComplete:(void(^)(NSDictionary* params))onComplete
{
    if (key) {
        self.handlers[key] = @{@"handled":@"0", @"handler" : [onComplete copy]}.mutableCopy;
    }
}


-(void)postNotification:(NSString*)title withAction:(NSString*)actionTitle onDate:(NSDate*)fireDate uniqueKey:(NSString*)key
{
    [self postNotification:title withAction:actionTitle onDate:fireDate uniqueKey:key withCustomParameters:@{}];
}

-(void)postNotification:(NSString*)title withAction:(NSString*)actionTitle onDate:(NSDate*)fireDate uniqueKey:(NSString*)key withCustomParameters:(NSDictionary*)customParameter;
{
    [self postNotification:title withAction:actionTitle onDate:fireDate uniqueKey:key withCustomParameters:customParameter andBadgeNumber:0];
}

-(void)postNotification:(NSString*)title withAction:(NSString*)actionTitle onDate:(NSDate*)fireDate uniqueKey:(NSString*)key withCustomParameters:(NSDictionary*)customParameter andBadgeNumber:(NSInteger)badgeNumber
{
    [self postNotification:title withAction:actionTitle onDate:fireDate uniqueKey:key withCustomParameters:customParameter andBadgeNumber:badgeNumber repeat:NO repeatUnit:NSCalendarUnitDay];
}

-(void)postNotification:(NSString*)title withAction:(NSString*)actionTitle onDate:(NSDate*)fireDate uniqueKey:(NSString*)key withCustomParameters:(NSDictionary*)customParameter andBadgeNumber:(NSInteger)badgeNumber repeat:(BOOL)repeat repeatUnit:(NSCalendarUnit)repeatUnit
{
    if (!customParameter) {
        customParameter = @{};
    }
    
    __weak typeof(self) weakSelf = self;
    
    [self notioficationAlreadyExistWithKey:key withCompletionHandler:^(BOOL isNotificationAlreadyExist) {
        if (!isNotificationAlreadyExist) {
            if (([[[UIDevice currentDevice] systemVersion] floatValue] >= 10.0)) {
                UNMutableNotificationContent *localNotification = [UNMutableNotificationContent new];
                
                localNotification.title = [NSString localizedUserNotificationStringForKey:actionTitle arguments:nil];
                
                localNotification.body = [NSString localizedUserNotificationStringForKey:title arguments:nil];
                
                localNotification.sound = [UNNotificationSound defaultSound];
                
                NSDictionary *infoDict = @{@"key":key, @"customParameters": customParameter};
                localNotification.userInfo = infoDict;
                
                
                NSCalendar *calendar = [NSCalendar currentCalendar];
                NSDateComponents *dateComponents = [calendar components:(NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear | NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond) fromDate:fireDate];
                
                
                NSDateComponents *fireDateComponents = nil;
                
                NSDate *now = [NSDate date];
                if ([fireDate compare:now] == NSOrderedAscending) {
                    NSLog(@"fireDate is earlier than date2");
                    fireDateComponents = [[NSDateComponents alloc]init];
                    [fireDateComponents setHour:dateComponents.hour];
                    [fireDateComponents setMinute:dateComponents.minute];
                    [fireDateComponents setSecond:dateComponents.second];
                }
                else{
                    NSLog(@"fireDate is later than date2");
                    fireDateComponents = [calendar components:(NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear | NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond) fromDate:fireDate];
                }
                
                /*
                 if ([fireDate compare:now] == NSOrderedDescending) {
                 NSLog(@"fireDate is earlier than date2");
                 } else if ([fireDate compare:now] == NSOrderedAscending) {
                 NSLog(@"fireDate is earlier than date2");
                 } else {
                 NSLog(@"dates are the same");
                 }
                 */
                
                
                UNCalendarNotificationTrigger *trigger = [UNCalendarNotificationTrigger triggerWithDateMatchingComponents:fireDateComponents repeats:repeat];
                
                localNotification.badge = @(badgeNumber);
                
                UNNotificationRequest *request = [UNNotificationRequest requestWithIdentifier:key content:localNotification trigger:trigger];
                
                UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
                
                
                [center addNotificationRequest:request withCompletionHandler:^(NSError * _Nullable error) {
                    NSLog(@"Notification created");
                }];
                
                if (weakSelf.handlers[key]) {
                    NSMutableDictionary *dict = self.handlers[key];
                    dict[@"handled"] = @"0";
                }
                
                return;
            }
            
            
            UILocalNotification *localNotif = [[UILocalNotification alloc] init];
            if (localNotif == nil){
                return;
            }
            
            localNotif.fireDate = fireDate;
            localNotif.timeZone = [NSTimeZone defaultTimeZone];
            
            localNotif.alertBody = title;
            localNotif.alertAction = actionTitle;
            
            localNotif.soundName = UILocalNotificationDefaultSoundName;
            localNotif.applicationIconBadgeNumber = badgeNumber;
            
            NSDictionary *infoDict = @{@"key":key, @"customParameters": customParameter};
            localNotif.userInfo = infoDict;
            
            if (repeat) {
                localNotif.repeatInterval = repeatUnit;
            }
            
            [[UIApplication sharedApplication] scheduleLocalNotification:localNotif];
            
            if (weakSelf.handlers[key]) {
                NSMutableDictionary *dict = self.handlers[key];
                dict[@"handled"] = @"0";
            }
        }
    }];
}

-(void)cancelNotificationWithKey:(NSString*)key;
{
    if (([[[UIDevice currentDevice] systemVersion] floatValue] >= 10.0)) {
        UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
        [center removePendingNotificationRequestsWithIdentifiers:@[key]];
        
    }
    else{
        for (UILocalNotification *notification in [UIApplication sharedApplication].scheduledLocalNotifications) {
            NSDictionary *userinfo = notification.userInfo;
            if ([userinfo[@"key"] isEqualToString:key]) {
                [[UIApplication sharedApplication] cancelLocalNotification:notification];
                break;
            }
        }
    }
}

-(void)cancelAllLocalNotifications
{
    if (([[[UIDevice currentDevice] systemVersion] floatValue] >= 10.0)) {
        UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
        [center removeAllPendingNotificationRequests];
        
    }
    else{
        [[UIApplication sharedApplication] cancelAllLocalNotifications];
    }
}

#pragma mark - Internal Methods

-(void)notioficationAlreadyExistWithKey:(NSString*)key withCompletionHandler:(void(^)(BOOL isNotificationAlreadyExist))onCompletion
{
    if (([[[UIDevice currentDevice] systemVersion] floatValue] >= 10.0)) {
        UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
        [center getPendingNotificationRequestsWithCompletionHandler:^(NSArray<UNNotificationRequest *> * _Nonnull requests) {
            BOOL exist = FALSE;
            if (requests) {
                for (UNNotificationRequest *request in requests) {
                    if ([request.identifier isEqualToString:key]) {
                        exist = TRUE;
                        break;
                    }
                }
            }
            
            if (onCompletion) {
                onCompletion(exist);
            }
        }];
    }
    else{
        BOOL exist = FALSE;
        for (UILocalNotification *notification in [UIApplication sharedApplication].scheduledLocalNotifications) {
            NSDictionary *userinfo = notification.userInfo;
            if ([userinfo[@"key"] isEqualToString:key]) {
                [[UIApplication sharedApplication] cancelLocalNotification:notification];
                exist = TRUE;
            }
        }
        
        if (onCompletion) {
            onCompletion(exist);
        }
    }
}

#pragma mark - iOS 10 Push Notifications

//Called when a notification is delivered to a foreground app.
-(void)userNotificationCenter:(UNUserNotificationCenter *)center willPresentNotification:(UNNotification *)notification withCompletionHandler:(void (^)(UNNotificationPresentationOptions options))completionHandler
{
    NSLog(@"User Info : %@",notification.request.content.userInfo);
    completionHandler(UNAuthorizationOptionSound | UNAuthorizationOptionAlert | UNAuthorizationOptionBadge);
}

//Called to let your app know which action was selected by the user for a given notification.
-(void)userNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void(^)(void))completionHandler
{
    NSLog(@"User Info : %@",response.notification.request.content.userInfo);
    completionHandler();
    [self localNotificationReceived:response.notification.request.content.userInfo];
}

@end
