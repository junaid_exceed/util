//
//  GrowingTextView.h
//  Pods
//
//  Created by Junaid Akram on 14/09/2017.
//
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

@interface GrowingTextView : UITextView

@property (nonatomic) NSInteger maxLength;
@property (nonatomic) BOOL trimWhiteSpaceWhenEndEditing;

@property (nonatomic) CGFloat minHeight;
@property (nonatomic) CGFloat maxHeight;

@property (nonatomic, strong) NSString *placeHolder;
@property (nonatomic, strong) UIColor *placeHolderColor;
@property (nonatomic, strong) NSAttributedString *attributedPlaceHolder;
@property (nonatomic) CGFloat placeHolderLeftMargin;


@property (nonatomic, copy) void(^onChangeHeight)(CGFloat height);


@end
