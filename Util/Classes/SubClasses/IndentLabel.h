//
//  IndentLabel.h
//  IdenediClient
//
//  Created by Junaid Akram on 25/01/2017.
//  Copyright © 2017 Idenedi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IndentLabel : UILabel

@property (nonatomic) UIEdgeInsets textInsets;

@end
