//
//  GrowingTextView.m
//  Pods
//
//  Created by Junaid Akram on 14/09/2017.
//
//

#import "GrowingTextView.h"

@interface GrowingTextView ()
{
    NSString *oldText;
    CGSize oldSize;
}

@property (nonatomic, weak) IBOutlet NSLayoutConstraint *heightConstraint;

@end

@implementation GrowingTextView

#pragma mark -- Getter / Setter

-(void)setMinHeight:(CGFloat)minHeight
{
    _minHeight = minHeight;
    [self forceLayoutSubviews];
}

-(void)setMaxHeight:(CGFloat)maxHeight
{
    _maxHeight = maxHeight;
    [self forceLayoutSubviews];
}

-(void)setPlaceHolder:(NSString *)placeHolder
{
    _placeHolder = placeHolder;
    [self setNeedsDisplay];
}

-(void)setPlaceHolderColor:(UIColor *)placeHolderColor
{
    _placeHolderColor = placeHolderColor;
    [self setNeedsDisplay];
}

-(void)setAttributedPlaceHolder:(NSAttributedString *)attributedPlaceHolder
{
    _attributedPlaceHolder = attributedPlaceHolder;
    [self setNeedsDisplay];
}

-(void)setPlaceHolderLeftMargin:(CGFloat)placeHolderLeftMargin
{
    _placeHolderLeftMargin = placeHolderLeftMargin;
    [self setNeedsDisplay];
}

-(void)forceLayoutSubviews
{
    oldSize = CGSizeZero;
    [self setNeedsLayout];
    [self layoutIfNeeded];
}

-(void)associateConstraints
{
    for (NSLayoutConstraint *constraint in self.constraints) {
        if (constraint.firstAttribute == NSLayoutAttributeHeight) {
            if (constraint.relation == NSLayoutRelationEqual) {
                _heightConstraint = constraint;
            }
        }
    }
}

-(void)commonInit
{
    self.contentMode = UIViewContentModeRedraw;
    [self associateConstraints];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textDidChange:) name:UITextViewTextDidChangeNotification object:self];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textDidEndEditing:) name:UITextViewTextDidEndEditingNotification object:self];
}

-(void)deInit
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    [self initializeValues];
    [self commonInit];
    return self;
}

-(instancetype)initWithFrame:(CGRect)frame textContainer:(NSTextContainer *)textContainer
{
    self = [super initWithFrame:frame textContainer:textContainer];
    [self initializeValues];
    [self commonInit];
    return self;
}

-(void)initializeValues
{
    _maxLength = 0;
    _trimWhiteSpaceWhenEndEditing = TRUE;
    _placeHolderColor = [UIColor colorWithWhite:0.8 alpha:1.0];
    _placeHolderLeftMargin = 5.0f;
}

-(instancetype)init
{
    self = [super init];
    if (self) {
        [self initializeValues];
    }
    return self;
}

-(void)scrollToCorrectPosition
{
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(DISPATCH_TIME_NOW + NSEC_PER_MSEC * 100)), dispatch_get_main_queue(), ^{
        if ([self isFirstResponder]) {
            [self scrollRangeToVisible:NSMakeRange(-1, 0)];
        }
        else{
            [self scrollRangeToVisible:NSMakeRange(0, 0)];
        }
    });
}

-(void)layoutSubviews
{
    [super layoutSubviews];
    
    if ([self.text isEqualToString:oldText] && CGSizeEqualToSize(self.bounds.size, oldSize)) {
        return;
    }
    
    oldText = self.text;
    oldSize = self.bounds.size;
    
    CGSize size = [self sizeThatFits:CGSizeMake(self.bounds.size.width, FLT_MAX)];
    CGFloat height = size.height;
    
    height = _minHeight > 0 ? fmax(height, _minHeight) : height;
    
    height = _maxHeight > 0 ? fmin(height, _maxHeight) : height;
    
    if (!_heightConstraint) {
        _heightConstraint = [NSLayoutConstraint constraintWithItem:self attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:height];
        [self addConstraint:_heightConstraint];
    }
    
    if (height != _heightConstraint.constant) {
        _heightConstraint.constant = height;
        [self scrollToCorrectPosition];
        
        if (self.onChangeHeight) {
            self.onChangeHeight(height);
        }
    }
    
}


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
    [super drawRect:rect];
    if ([self.text length] == 0) {
        CGFloat xValue = self.textContainerInset.left + _placeHolderLeftMargin;
        CGFloat width = rect.size.width - xValue - self.textContainerInset.right;
        CGFloat height = rect.size.height - self.textContainerInset.top - self.textContainerInset.bottom;
        CGFloat yValue = (rect.size.height - height) / 2;
        
        CGRect placeHolderRect = CGRectMake(xValue, yValue, width, height);
        
        if (_attributedPlaceHolder) {
            [_attributedPlaceHolder drawInRect:placeHolderRect];
        }
        else if (_placeHolder){
            NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc]init];
            paragraphStyle.alignment = NSTextAlignmentLeft;
            NSMutableDictionary *attributes = [NSMutableDictionary dictionaryWithDictionary:@{NSForegroundColorAttributeName : _placeHolderColor,
                                                                                              NSParagraphStyleAttributeName : paragraphStyle}];
            if (self.font) {
                attributes[NSFontAttributeName] = self.font;
            }
            
            [_placeHolder drawInRect:placeHolderRect withAttributes:attributes];
        }
    }
}

-(void)textDidEndEditing:(NSNotification*)notification
{
    if (_trimWhiteSpaceWhenEndEditing) {
        self.text = [self.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        [self setNeedsDisplay];
    }
    [self scrollToCorrectPosition];
    
}

- (void)textDidChange:(NSNotification*)notification
{
    if (_maxLength > 0 && [self.text length] > _maxLength) {
        self.text = [self.text substringWithRange:NSMakeRange(0, _maxLength)];
        [self.undoManager removeAllActions];
    }
    
    [self setNeedsDisplay];
    
}

@end
