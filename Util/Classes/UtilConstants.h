//
//  UtilConstants.h
//  Pods
//
//  Created by Junaid Akram on 23/09/2020.
//

#ifndef UtilConstants_h
#define UtilConstants_h

#define UtilLog(fmt, ...) //NSLog((@"%s [Line %d] " fmt), __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__);
#define UtilLogError(fmt, ...) NSLog((@"❗:%s [Line %d] " fmt), __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__);
#define UtilLogTracking(fmt, ...) NSLog((@"TR:%s [Line %d] " fmt), __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__);


#endif /* UtilConstants_h */
