//
//  UtilImage.h
//  Pods
//
//  Created by Junaid Akram on 27/08/2017.
//
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "UIImageView+UtilImage.h"

#ifndef UTILIMAGEGMACROS
#define UTILIMAGEGMACROS
#define UTILIMAGE(file) [UtilImage image:file]
#endif

@interface UtilImage : NSObject
{
    NSMutableDictionary* downloadHeaders;
}

@property(nonatomic, retain) NSMutableDictionary* downloadHeaders;

+(UtilImage*)shared;

+(UIImage*)image:(NSString*)file;
+(UIImage*)image:(NSString*)file cache:(BOOL)doCache;
+(UIImage*)image:(NSString*)file cache:(BOOL)doCache left:(int)left top:(int)top;
+(UIImage*)image:(NSString*)filename left:(int)left top:(int)top;

+(void)loadImage:(NSString*)file;
+(void)loadImage:(NSString*)file onComplete:(void(^)(UIImage*))onComp;
+(void)loadImage:(NSString*)file assignTo:(UIImageView*)imageView;
+(void)loadImage:(NSString*)file assignTo:(UIImageView*)imageView placeholder:(NSString*)placeholder;


// Cache methods
+(void)cacheImage:(UIImage*)image forKey:(NSString*)key;
+(void)clearCache;
+(void)clearDiskCache;

@end
