//
//  UtilMacros.h
//  Pods
//
//  Created by Junaid Akram on 28/04/2017.
//
//

#define SYNTHESIZE_SINGLETON_FOR_CLASS(classname) \
\
static classname *shared##classname = nil; \
\
+ (classname *)shared##classname \
{ \
@synchronized(self) \
{ \
if (shared##classname ==  nil) { \
NSString* classStr = [NSString stringWithFormat:@"%@Lua", @#classname]; \
Class luaClass = NSClassFromString(classStr); \
if(luaClass) { \
Log(@"Loading lua class %@", classStr); \
shared##classname = [[luaClass alloc] init]; \
Log(@"Loaded: %@", shared##classname);\
} else { \
Log(@"Loading class %@", @#classname); \
[[self alloc] init]; \
}\
} \
} \
\
return shared##classname; \
} \
\
+ (id)allocWithZone:(NSZone *)zone \
{ \
@synchronized(self) \
{ \
if (shared##classname == nil) \
{ \
shared##classname = [super allocWithZone:zone]; \
return shared##classname; \
} \
} \
\
return nil; \
} \
\
- (id)copyWithZone:(NSZone *)zone \
{ \
return self; \
} \
\
- (id)retain \
{ \
return self; \
} \
\
- (NSUInteger)retainCount \
{ \
return NSUIntegerMax; \
} \
\
- (void)release \
{ \
} \
\
- (id)autorelease \
{ \
return self; \
}


/*!
 * @function Singleton GCD Macro
 */
#ifndef SYNTHESIZE_ARC_SINGLETON_FOR_CLASS
#define SYNTHESIZE_ARC_SINGLETON_FOR_CLASS(classname)                        \
\
+ (classname *)shared {                      \
\
static dispatch_once_t pred;                        \
__strong static classname * shared##classname = nil;\
dispatch_once( &pred, ^{                            \
shared##classname = [[self alloc] init];    \
});\
return shared##classname;                           \
}
#endif

//========================================================
// Memory Macros
//========================================================
#define safeRelease(x) \
if(x) { \
[x release]; \
x = nil; \
}


//========================================================
// Rect
//========================================================
#define UtilRectSetX(rect, x) \
{rect = CGRectMake(x, rect.origin.y, rect.size.width, rect.size.height);}

#define UtilRectSetY(rect, y) \
{rect = CGRectMake(rect.origin.x, y, rect.size.width, rect.size.height);}

#define UtilRectSetWidth(rect, width) \
{rect = CGRectMake(rect.origin.x, rect.origin.y, width, rect.size.height);}

#define UtilRectSetHeight(rect, height) \
{rect = CGRectMake(rect.origin.x, rect.origin.y, rect.size.width, height);}


#define IS_IOS7()   (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1)
#define IS_IPAD     (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
#define IS_IPHONE   (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define IS_IPHONE_5 (fabs((double)[[UIScreen mainScreen] bounds].size.height - (double)568) < DBL_EPSILON)
