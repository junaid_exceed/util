//
//  Util.h
//  Pods
//
//  Created by Junaid Akram on 26/01/2017.
//
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface Util : NSObject

+(BOOL)fileExistsAtPath:(NSString*)path;
+(BOOL)folderExistsAtPath:(NSString*)path;
+(BOOL)fileExistsWithName:(NSString*)name;

+(NSString*)resourcePath:(NSString*) fileName;

+(NSString*)applicationDocumentsDirectory;
+(NSString*)applicationCacheDirectory;
+(NSString*)applicationLibraryDirectory;

+ (void)markiCloudBackupFlagForDirectoryPath:(NSString*)directoryPath backupFlag:(BOOL)backupFlag withCompletionHandler:(void(^)(BOOL success))onCompletion;

+(id)objectFormDictionary:(NSDictionary*)dictionary withKey:(NSString*)key;


#pragma mark - Utility Function

+(void)makeCallActionForNumber:(NSString*)number;
+(void)openWebsiteWithAddress:(NSString*)websiteAddress;
+(void)openLocationOnMapForlatitude:(CGFloat)latitude andlongitude:(CGFloat)longitude;

#pragma mark - Auto Layout

+(CGFloat)adaptiveDeviceRatioPercentage;

+(void)adjustFontOfLabelWithRespectToScreen:(UILabel*)label;

+(void)adjustFontOfTextFiledWithRespectToScreen:(UITextField*)textField;

+(void)adjustFontOfButtonWithRespectToScreen:(UIButton*)button;

+(void)adjustConstantOfConstraint:(NSLayoutConstraint*)constraint;

+(void)adjustSubViewsFontForSuperView:(UIView*)superView;

+(void)adjustSubViewsFontForAllSubViews:(UIView*)superView;

+(void)adjustSuperViewConstraintConstantWithIdentifier:(NSString*)identifer forView:(UIView*)view;

+(CGFloat)adaptiveFontSize:(CGFloat)fontSize;

#pragma mark - Download & Upload

//+ (void)downloadAndSaveFileWithName:(NSString*)fileName FromURL:(NSString*)urlString withProgress:(void(^)(NSProgress *progress))downloadedProgress withCompletionHandler:(void(^)(NSURL *filePath, NSError *error))onCompletion;

@end
