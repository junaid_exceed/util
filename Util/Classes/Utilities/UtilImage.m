//
//  UtilImage.m
//  Pods
//
//  Created by Junaid Akram on 27/08/2017.
//
//

#import "UtilImage.h"
#import "UtilMacros.h"
#import "UtilImageLocalCache.h"
#import "NSThread+Util.h"
#import "Util.h"
#import "ASIHTTPRequest.h"

@implementation UtilImage
//=========================================================================
#pragma mark -
#pragma mark Properties
//=========================================================================
@synthesize downloadHeaders;


SYNTHESIZE_ARC_SINGLETON_FOR_CLASS(UtilImage);
//=========================================================================
#pragma mark -
#pragma mark CONSTANTS
//=========================================================================


//=========================================================================
#pragma mark -
#pragma mark Custome Getters/Setters
//=========================================================================

//=========================================================================
#pragma mark -
#pragma mark Initialization
//=========================================================================
-(id)init {
    self = [super init];
    self.downloadHeaders = [NSMutableDictionary dictionary];
    return self;
}



//=========================================================================
#pragma mark -
#pragma mark Image functions
//=========================================================================
-(UIImage*)image:(NSString*)filename cache:(BOOL)doCache {
    UIImage* image = [[UtilImageLocalCache sharedImageLocalCache] imageFromKey:filename];
    BOOL toDisk = doCache;
    if (!image && filename) {
        // Checking if it is a url
        if ([[filename lowercaseString] hasPrefix:@"http://"]) {
            // Loading from web
            ASIHTTPRequest* request = [ASIHTTPRequest requestWithURL:[NSURL URLWithString:filename]];
            NSDateFormatter *format = [[NSDateFormatter alloc] init];
            format.dateFormat = @"yyyyMMMddHHmmss";
            NSString *dateString = [format stringFromDate:[NSDate date]];
            
            NSString* tempfilename = [NSString stringWithFormat:@"%@_%d.temp", dateString, rand()];
            NSString* filePath = [[Util applicationCacheDirectory] stringByAppendingPathComponent:tempfilename];
            [request setDownloadDestinationPath:filePath];
            //[request addRequestHeader:@"User-Agent" value:USER_AGENT];
            [request startSynchronous];
            if (![request error]) {
                // success
                image = [UIImage imageWithContentsOfFile:filePath];
                // Now delete the temp file
                [[NSFileManager defaultManager] removeItemAtPath:filePath error:nil];
            }
        } else {
            // Checking if exists in resources
            image = [UIImage imageWithContentsOfFile:[Util resourcePath:filename]];
            if (!image) {
                image = [UIImage imageWithContentsOfFile:filename];
            }
            if (!image) {
                image = [UIImage imageNamed:filename];
            }
            toDisk = NO;
        }
        // Cache if needed
        if (image && doCache) {
            [[UtilImageLocalCache sharedImageLocalCache] storeImage:image forKey:filename toDisk:toDisk];
        }
    }
    return  image;
}


-(UIImage*)image:(NSString*)filename cache:(BOOL)doCache left:(int)left top:(int)top {
    NSString* key = nil;
    UIImage* image = nil;
    if (filename) {
        key = [NSString stringWithFormat:@"%@__%dx%d",filename,left,top];
        image = [[UtilImageLocalCache sharedImageLocalCache] imageFromKey:key];
        if (!image) {
            UIImage* img = [self image:filename cache:NO];
            image = [img stretchableImageWithLeftCapWidth:left topCapHeight:top];
            if (image && doCache) {
                [[UtilImageLocalCache sharedImageLocalCache] storeImage:image forKey:key toDisk:NO];
            }
        }
    }
    return image;
}


//=========================================================================
#pragma mark -
#pragma mark Static wrappers
//=========================================================================
+(UIImage*)image:(NSString*)filename {
    if (!filename) {
        return nil;
    }
    return [UtilImage image:filename cache:YES];
}


+(UIImage*)image:(NSString*)filename cache:(BOOL)doCache {
    if (!filename) {
        return nil;
    }
    return [[UtilImage shared] image:filename cache:doCache];
}


+(UIImage*)image:(NSString*)filename left:(int)left top:(int)top {
    if (!filename) {
        return nil;
    }
    return [self image:filename cache:YES left:left top:top];
}

+(UIImage*)image:(NSString*)filename cache:(BOOL)doCache left:(int)left top:(int)top {
    if (!filename) {
        return nil;
    }
    return [[UtilImage shared] image:filename cache:doCache left:left top:top];
}


+(void)loadImage:(NSString*)file {
    if (!file) {
        return;
    }
    [UtilImage image:file cache:YES];
}


+(void)loadImage:(NSString*)file onComplete:(void(^)(UIImage*))onComp
{
    [NSThread performBlockInBackground:^() {
        if (file) {
            UIImage* img = [UtilImage image:file cache:YES];
            if (onComp) {
                onComp(img);
            }
        } else {
            if (onComp) {
                onComp(nil);
            }
        }
    }];
}


+(void)loadImage:(NSString*)file assignTo:(UIImageView*)imageView {
    [self loadImage:file assignTo:imageView placeholder:nil];
}


+(void)loadImage:(NSString*)file assignTo:(UIImageView*)imageView placeholder:(NSString*)placeholder {
    if (imageView && [imageView isKindOfClass:[UIImageView class]]) {
        if (file) {
            NSURL* url = [NSURL URLWithString:file];
            if ([file hasPrefix:@"file://"]) {
                url = [NSURL fileURLWithPath:file];
            } else  if ([file hasPrefix:@"/var/"]) {
                url = [NSURL fileURLWithPath:file];
            }
            [NSThread performBlockInBackground:^{
                [imageView setimageWithURL:url placeholderImage:UTILIMAGE(placeholder)];
            }];
        }
    }
}


//=========================================================================
#pragma mark -
#pragma mark Cache methods
//=========================================================================
+(void)cacheImage:(UIImage*)image forKey:(NSString*)key {
    if (image && [image isKindOfClass:[UIImage class]]
        && key && [key isKindOfClass:[NSString class]]) {
        [[UtilImageLocalCache sharedImageLocalCache] storeImage:image forKey:key toDisk:YES];
    }
}

+(void)clearCache {
    [[UtilImageLocalCache sharedImageLocalCache] clearMemory];
}


+(void)clearDiskCache {
    [[UtilImageLocalCache sharedImageLocalCache] clearDisk];
}

@end
