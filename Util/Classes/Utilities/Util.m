//
//  Util.m
//  Pods
//
//  Created by Junaid Akram on 26/01/2017.
//
//

#import "Util.h"
//#import "AFNetworking.h"

@implementation Util

+(BOOL) fileExistsAtPath:(NSString*) path
{
    if (path && [path hasPrefix:@"file://"]) {
        return [[NSFileManager defaultManager] fileExistsAtPath:[[NSURL URLWithString:path] path]];
    } else {
        return [[NSFileManager defaultManager] fileExistsAtPath:path];
    }
}

+(BOOL)folderExistsAtPath:(NSString*) path
{
    BOOL isFolder = NO;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if ([fileManager fileExistsAtPath:path isDirectory:&isFolder]) {
        if (isFolder) {
            return YES;
        }
    }
    return NO;
}

+(BOOL)fileExistsWithName:(NSString*) name
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *filePath = [documentsDirectory stringByAppendingPathComponent:name];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if (![fileManager fileExistsAtPath:documentsDirectory]) {
        NSError *error = nil;
        [fileManager createDirectoryAtPath:documentsDirectory withIntermediateDirectories:YES attributes:nil error:&error];
    }
    return [fileManager fileExistsAtPath:filePath];
}


static NSString* resourcePath = nil;
+(NSString*)resourcePath:(NSString*) fileName
{
    if (!resourcePath) {
        resourcePath = [[NSBundle mainBundle] resourcePath];
    }
    return [resourcePath stringByAppendingPathComponent:fileName];
}



+ (NSString *)applicationDocumentsDirectory
{
    return [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
}

+(NSString*)applicationCacheDirectory
{
    return [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) lastObject];
}

+(NSString*)applicationLibraryDirectory
{
    return [NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES) lastObject];
}


+ (void)markiCloudBackupFlagForDirectoryPath:(NSString*)directoryPath backupFlag:(BOOL)backupFlag withCompletionHandler:(void(^)(BOOL success))onCompletion;
{
    NSURL *urlFromPath = [[NSURL alloc] initFileURLWithPath:directoryPath];
    
    assert([[NSFileManager defaultManager] fileExistsAtPath: [urlFromPath path]]);
    
    NSError *error = nil;
    BOOL success = [urlFromPath setResourceValue: [NSNumber numberWithBool: backupFlag] forKey: NSURLIsExcludedFromBackupKey error: &error];
    
    if (onCompletion) {
        onCompletion(success);
    }
}

+(id)objectFormDictionary:(NSDictionary *)dictionary withKey:(NSString *)key
{
    if (dictionary && ![dictionary isEqual:[NSNull null]] && [dictionary isKindOfClass:[NSDictionary class]]) {
        id obj = dictionary[key];
        //        NSLog(@"%@", [obj class]);
        if ([obj isKindOfClass:[NSString class]]) {
            if ([obj isEqualToString:@"<null>"]) {
                obj  = @"";
            }
        }
        return (obj == nil || [obj isEqual:[NSNull null]]) ? @"" : obj ;
    }
    return @"";
}

#pragma mark - Auto Layout

+(CGSize)screenSize
{
    return [UIScreen mainScreen].bounds.size;
}

+(CGFloat)percentage
{
    CGSize screenSize = [self screenSize];
    
    CGFloat percentage = 0;
    
    if (screenSize.width == 375) {
        percentage = .90;
    }
    else if (screenSize.width == 320 && screenSize.height == 568) {
        percentage = .77;
    }
    else if (screenSize.width == 320 && screenSize.height == 480) {
        percentage = .65;
    }
    return percentage;
}

+(CGFloat)adaptiveFontSize:(CGFloat)fontSize
{
    CGFloat percentage = [self percentage];;
    if (percentage > 0) {
        fontSize = fontSize * percentage;
    }
    return fontSize;
}

+(void)adjustSubViewsFontForSuperView:(UIView*)superView
{
    CGFloat percentage = [self percentage];;
    if (percentage > 0) {
        for (id view in superView.subviews) {
            if ([view isMemberOfClass:[UILabel class]]) {
                [self adjustFontOfLabelWithRespectToScreen:view];
            }
            else if ([view isMemberOfClass:[UIButton class]]){
                [self adjustFontOfButtonWithRespectToScreen:view];
            }
            else if ([view isMemberOfClass:[UITextField class]]){
                [self adjustFontOfTextFiledWithRespectToScreen:view];
            }
        }
    }
}


+(void)adjustSubViewsFontForAllSubViews:(UIView*)superView
{
    CGFloat percentage = [self percentage];;
    if (percentage > 0) {
        for (id view in superView.subviews) {
            //            NSLog(@"%@", [view class]);
            if ([view isMemberOfClass:[UIView class]]) {
                [self adjustSubViewsFontForAllSubViews:view];
            }
            else if ([view isMemberOfClass:[UILabel class]]) {
                [self adjustFontOfLabelWithRespectToScreen:view];
            }
            else if ([view isMemberOfClass:[UIButton class]]){
                [self adjustFontOfButtonWithRespectToScreen:view];
            }
            else if ([view isMemberOfClass:[UITextField class]]){
                [self adjustFontOfTextFiledWithRespectToScreen:view];
            }
        }
    }
}

+(void)adjustSuperViewConstraintConstantWithIdentifier:(NSString*)identifer forView:(UIView*)view
{
    CGFloat percentage = [self percentage];;
    if (percentage > 0) {
        for (NSLayoutConstraint *constraint in view.superview.constraints) {
            if ([constraint.identifier isEqualToString:identifer]) {
                constraint.constant = (constraint.constant * percentage);
            }
        }
    }
}

+(CGFloat)adaptiveDeviceRatioPercentage
{
    return [self percentage];
}


+(void)adjustFontOfLabelWithRespectToScreen:(UILabel*)label
{
    CGFloat percentage = [self percentage];;
    if (percentage > 0) {
        UIFont *newFont = label.font;
        label.font = [UIFont fontWithName:newFont.familyName size:(newFont.pointSize * percentage)];
    }
}

+(void)adjustFontOfTextFiledWithRespectToScreen:(UITextField*)textField
{
    CGFloat percentage = [self percentage];;
    if (percentage > 0) {
        UIFont *newFont = textField.font;
        textField.font = [UIFont fontWithName:newFont.familyName size:(newFont.pointSize * percentage)];
    }
}

+(void)adjustFontOfButtonWithRespectToScreen:(UIButton*)button
{
    CGFloat percentage = [self percentage];;
    if (percentage > 0) {
        UIFont *newFont = button.titleLabel.font;
        button.titleLabel.font = [UIFont fontWithName:newFont.familyName size:(newFont.pointSize * percentage)];
    }
}

+(void)adjustConstantOfConstraint:(NSLayoutConstraint*)constraint
{
    CGFloat percentage = [self percentage];;
    if (percentage > 0) {
        CGFloat consta = constraint.constant;
        consta = consta * percentage;
        constraint.constant = consta ;
    }
}

#pragma mark - Utility Function

+(BOOL)makeActionForProtocol:(NSString*)protocol withValue:(NSString*)value
{
    NSString *UrlString = [NSString stringWithFormat:@"%@%@",protocol,value];
    NSURL *url = [NSURL URLWithString:[UrlString stringByAddingPercentEncodingWithAllowedCharacters:NSCharacterSet.URLQueryAllowedCharacterSet]];
    
    if ([[UIApplication sharedApplication] openURL:url]) {
        return YES;
    }
    else{
        return NO;
    }
}

+(void)makeCallActionForNumber:(NSString*)number
{
    NSString *protocol = @"tel:";
    [self makeActionForProtocol:protocol withValue:number];
}

+(void)openWebsiteWithAddress:(NSString*)websiteAddress
{
    NSString *protocol = @"http://";
    if ([websiteAddress rangeOfString:protocol].location != NSNotFound) {
        protocol = @"";
    }
    [self makeActionForProtocol:protocol withValue:websiteAddress];
}

+(void)openLocationOnMapForlatitude:(CGFloat)latitude andlongitude:(CGFloat)longitude
{
    NSString* directionsURL = [NSString stringWithFormat:@"http://maps.apple.com/?ll=%f,%f",latitude, longitude];
    NSURL *url = [NSURL URLWithString:[directionsURL stringByAddingPercentEncodingWithAllowedCharacters:NSCharacterSet.URLQueryAllowedCharacterSet]];
    [[UIApplication sharedApplication] openURL:url];
}

#pragma mark - Download & Upload

//+ (void)downloadAndSaveFileWithName:(NSString*)fileName FromURL:(NSString*)urlString withProgress:(void(^)(NSProgress *progress))downloadedProgress withCompletionHandler:(void(^)(NSURL *filePath, NSError *error))onCompletion
//{
//    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
//    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
//
//    NSURL *URL = [NSURL URLWithString:urlString];
//    NSURLRequest *request = [NSURLRequest requestWithURL:URL];
//
//
////    NSURLSessionDownloadTask *downloadTask = [manager downloadTaskWithRequest:request progress:^(NSProgress * _Nonnull downloadProgress) {
////        <#code#>
////    } destination:^NSURL * _Nonnull(NSURL * _Nonnull targetPath, NSURLResponse * _Nonnull response) {
////        <#code#>
////    } completionHandler:^(NSURLResponse * _Nonnull response, NSURL * _Nullable filePath, NSError * _Nullable error) {
////        <#code#>
////    }];
//
//    NSURLSessionDownloadTask *downloadTask = [manager downloadTaskWithRequest:request progress:^(NSProgress * downloadProgress) {
//        if (downloadedProgress) {
//            downloadedProgress(downloadProgress);
//        }
//    } destination:^NSURL * _Nonnull(NSURL * _Nonnull targetPath, NSURLResponse * _Nonnull response) {
//        NSURL *documentsDirectoryURL = [[NSFileManager defaultManager] URLForDirectory:NSDocumentDirectory inDomain:NSUserDomainMask appropriateForURL:nil create:NO error:nil];
//        NSString *name = @"";
//        if (!fileName || [fileName length] == 0) {
//            NSString *urlString = [response.URL absoluteString];
//            NSArray *components = [urlString componentsSeparatedByString:@"/"];
//            name = [components lastObject];
//        }
//        else {
//            name = fileName;
//        }
//        return [documentsDirectoryURL URLByAppendingPathComponent:name];
//    } completionHandler:^(NSURLResponse * _Nonnull response, NSURL * _Nullable filePath, NSError * _Nullable error) {
//        NSLog(@"File downloaded to: %@", filePath);
//    }];
//
//    [downloadTask resume];
//}

@end
