#
# Be sure to run `pod lib lint Util.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'Util'
  s.version          = '0.1.2'
  s.summary          = 'Utility Classes'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
TODO: Add long description of the pod here.
                       DESC

  s.homepage         = 'https://bitbucket.org/junaid_exceed/util'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Junaid Akram' => 'junaid.exceed@gmail.com' }
  s.source           = { :git => 'https://junaid_exceed@bitbucket.org/junaid_exceed/util.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '9.0'

  s.source_files = 'Util/Classes/**/*'
  
  # s.resource_bundles = {
  #   'Util' => ['Util/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
s.dependency 'AFNetworking', '~> 4.0'
#s.dependency 'AFNetworking', '~> 3.0', :subspecs => ['Reachability', 'Serialization', 'Security', 'NSURLSession']
    s.dependency 'BlocksKit/Core', '~> 2.2'
s.dependency 'BlocksKit/DynamicDelegate', '~> 2.2'
s.dependency 'BlocksKit/MessageUI', '~> 2.2'
    s.dependency 'FMDB', '~> 2.6'
    s.dependency 'SOExtendedAttributes', '~> 1.0'
    s.dependency 'Reachability', '~> 3.2'
    s.dependency 'ASIHTTPRequest/CloudFiles', '~> 1.8'
s.dependency 'ASIHTTPRequest/Core', '~> 1.8'
s.dependency 'ASIHTTPRequest/S3', '~> 1.8'
end
